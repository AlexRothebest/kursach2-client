import Vue from 'vue'
import VueRouter from 'vue-router'

import ChangePassword from '@/components/auth/ChangePassword'
import Login from '@/components/auth/Login'
import Logout from '@/components/auth/Logout'
import SignUp from '@/components/auth/SignUp'
import Profile from '@/components/auth/Profile'

import Products from '@/components/Products'
import Basket from '@/components/Basket'
import Purchases from '@/components/Purchases'

import AddProductAdmin from '@/components/admin/AddProduct'
import CategoriesAdmin from '@/components/admin/Categories'
import EditProductAdmin from '@/components/admin/EditProduct'
import ManufacturersAdmin from '@/components/admin/Manufacturers'
import ProductsAdmin from '@/components/admin/Products'
import ReportsAdmin from '@/components/admin/Reports'
import StatisticsAdmin from '@/components/admin/Statistics'

import Error404 from '@/components/Error404'

Vue.use(VueRouter)

const routes = [
	{
		path: '/login',
		name: 'Login',
		component: Login
	},
	{
		path: '/sign_up',
		name: 'Sign Up',
		component: SignUp
	},
	{
		path: '/logout',
		name: 'Logout',
		component: Logout
	},
	{
		path: '/profile',
		name: 'Profile',
		component: Profile
	},
	{
		path: '/change_password',
		name: 'Change Password',
		component: ChangePassword
	},

	{
		path: '/',
		name: 'Home',
		redirect: { name: 'Products' }
	},
	{
		path: '/products',
		name: 'Products',
		component: Products
	},
	{
		path: '/basket',
		name: 'Basket',
		component: Basket
	},
	{
		path: '/purchases',
		name: 'Purchases',
		component: Purchases
	},

	{
		path: '/admin',
		name: 'Admin',
		redirect: { name: 'Products Admin' }
	},
	{
		path: '/admin/products',
		name: 'Products Admin',
		component: ProductsAdmin
	},
	{
		path: '/admin/products/add',
		name: 'Add Product Admin',
		component: AddProductAdmin
	},
	{
		path: '/admin/products/edit/:productId',
		name: 'Edit Product Admin',
		component: EditProductAdmin
	},
	{
		path: '/admin/categories',
		name: 'Categories Admin',
		component: CategoriesAdmin
	},
	{
		path: '/admin/manufacturers',
		name: 'Manufacturers Admin',
		component: ManufacturersAdmin
	},
	{
		path: '/admin/statistics',
		name: 'Statistics Admin',
		component: StatisticsAdmin
	},
	{
		path: '/admin/reports',
		name: 'Reports Admin',
		component: ReportsAdmin
	},

	{
		path: '*',
		name: '404',
		component: Error404
	}
]

const router = new VueRouter({
	mode: 'history',
	base: process.env.BASE_URL,
	routes
})

export default router
