import Vue from 'vue'
import Vuex from 'vuex'

import axios from 'axios'

import { serverURL } from '@/Config'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		userDataLoaded: false,
		user: {
			isAuthenticated: false,
			name: '',
			surname: '',
			email: '',
			phone: '',
			isAdmin: '',
			basket: {}
		},

		productsDataLoaded: false,
		products: {},
		categories: {},
		manufacturers: {},
		purchases: {}
	},
	mutations: {
		setUserData(state, data) {
			state.userDataLoaded = true;
			state.user = data.user;
		},

		setProductsData(state, data) {
			state.productsDataLoaded = true;
			state.products = data.products;
			state.categories = data.categories;
			state.manufacturers = data.manufacturers;
			state.purchases = data.purchases;
		},

		resetInitialState(state) {
			state.user = {
				isAuthenticated: false,
				name: '',
				surname: '',
				email: '',
				phone: '',
				isAdmin: ''
			}
		
			state.parcelsDataLoaded = false;
			state.parcels = {}
		}

	},
	actions: {
		async getUserData(state) {
			await axios.post(`${serverURL}/auth/get_user_data`, {}, {
				withCredentials: true
			}).then(response => {
				state.commit('setUserData', {
					user: {
						isAuthenticated: response.data.is_authenticated,
						name: response.data.name,
						surname: response.data.surname,
						email: response.data.email,
						phone: response.data.phone,
						isAdmin: response.data.is_admin,
						basket: response.data.basket
					}
				})
			})
		},

		async getProductsData(state) {
			await axios.post(`${serverURL}/products/all`, {}, {
				withCredentials: true
			}).then(response => {
				state.commit('setProductsData', {
					products: response.data.products,
					categories: response.data.categories,
					manufacturers: response.data.manufacturers,
					purchases: response.data.purchases
				})
			})
		},

		async resetInitialState(state) {
			state.commit('resetInitialState');
		}
	},
	modules: {},
	getters: {
		getUserDataLoaded: state => state.userDataLoaded,
		getUser: state => state.user,

		getProductsDataLoaded: state => state.productsDataLoaded,
		getProducts: state => state.products,
		getCategories: state => state.categories,
		getManufacturers: state => state.manufacturers,
		getPurchases: state => state.purchases
	}
})
