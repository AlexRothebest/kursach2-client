# This is the client side of my second year course work

## Setup

### Copy example of configuration file.

Windows:
```
copy src/Config.Example.js src/Config.js
```

Linux/MacOS:
```
cp src/Config.Example.js src/Config.js
```

### Now you have Config.js file in the "src" folder. Tune it.

### Install Node.js dependencies

```
npm install
```

### Run the project
```
npm run serve
```
